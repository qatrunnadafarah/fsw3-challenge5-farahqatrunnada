CREATE TABLE `Cars_Information` (
  `id` integer PRIMARY KEY,
  `type_name` varchar(255),
  `rent_per_day` int,
  `size` varchar(255),
  `image` varchar(255)
);
