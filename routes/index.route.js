const express = require('express');
const router = express.Router();

const carRoute = require('./cars.route');

// Cars
router.use('/cars', carRoute);

module.exports = router;