const express = require('express');
const Controller = require('../controllers/cars.controller');
const router = express.Router();

// Cars List
router.get('/', (req, res) => {
    Controller.getAll(req, res);
})

// Add New Car
router.get('/add-new-car', (req, res) => {
    res.render('add');
})

// Update Car
router.get('/update-car-information', (req, res) => {
    res.render('update');
})

router.post('/create', (req, res) => {
    Controller.create(req, res);
})

router.get('/:id', (req, res) => {
    Controller.getById(req, res);
})

router.get('/form-edit/:id', (req, res) => {
    Controller.formUpdate(req, res);
})  

router.post('/update-car-information/:id', (req, res) => {
    Controller.update(req, res);
})

router.get('/delete/:id', (req, res) => {
    Controller.delete(req, res);
})

module.exports = router;