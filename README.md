# Car Management Dashboard

| Method | Route              | Description                                                                      |
| :----- | :---------         | :------------------------------------------------------------------------------- |
| GET    | /cars              | Menampilkan landing page dan seluruh list mobil yang ada di database             |
| GET    | /cars/add-new-car  | Menampilkan page form untuk menambahkan informasi mobil baru                     |
| POST   | /cars/create       | Menambahkan data mobil baru ke database                                          |
| GET    | /cars/form-edit/:id| Menampilkan page form untuk mengupdate informasi mobil                           |
| POST   | /cars/update-car-information/:id  | Mengupdate perubahan informasi mobil                              |
| GET    | /cars/delete/:id   | Menghapus mobil dari database                                                    |

# Contoh Request Body (Create)

type_name:BMW
rent_per_day:440000
size:Small
image:https://images.hgmsites.net/med/2022-bmw-3-series-330i-sedan-angular-front-exterior-view_100823589_m.jpg

# Contoh Respon Body (Create)

{
    "id": 3,
    "type_name": "BMW",
    "rent_per_day": 440000,
    "size": "Small",
    "image": "https://images.hgmsites.net/med/2022-bmw-3-series-330i-sedan-angular-front-exterior-view_100823589_m.jpg",
    "updatedAt": "2022-04-21T11:49:06.119Z",
    "createdAt": "2022-04-21T11:49:06.119Z"
}
